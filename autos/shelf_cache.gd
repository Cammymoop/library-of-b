extends Node

var shelves_available: = 0
var cache: Array[Node3D] = []

func get_shelf() -> Node3D:
	var sh: Node3D = cache.pop_back()
	sh.position.y = 0
	remove_child(sh)
	shelves_available -= 1
	return sh

func take_shelf(shelf: Node3D) -> void:
	shelf.get_parent().remove_child(shelf)
	add_child(shelf)
	shelf.visible = false
	shelf.position.y = 1200
