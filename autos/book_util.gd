extends Node

var open_book: Node3D = null

func get_open_book() -> Node3D:
	return open_book

func set_open_book(b: Node3D) -> void:
	open_book = b

func new_open(b: Node3D) -> void:
	var old: = get_open_book()
	if old:
		old.close_me()
	
	set_open_book(b)
