extends Node

var prefix_calculated: = ""
var prefix_for: = -1
const space_numbers = [1, 2, 4, 8, 16, 32, 64, 128, 256]

func get_book_text(room_id: int, spiral_id: int, book_index: int) -> String:
	var the_str: = get_prefix(spiral_id) + get_postfix(book_index)
	return add_spaces(the_str, room_id, book_index)

func alpha(number: int) -> String:
	return char(97 + number)

func get_prefix(spiral_id: int) -> String:
	if prefix_for == spiral_id:
		return prefix_calculated
	prefix_for = spiral_id
	
	var reduced: = spiral_id
	prefix_calculated = ""
	for i in 8:
		prefix_calculated += alpha(reduced % 26)
		reduced = floori(reduced / 26)
	
	return prefix_calculated

func get_postfix(book_index: int) -> String:
	return alpha(floori(book_index / 26)) + alpha(book_index % 26)

func add_spaces(the_str: String, room_id: int, book_index: int) -> String:
	var modified_index = (book_index - room_id) % 512
	for i in range(8, -1, -1):
		if space_numbers[i] & modified_index > 0:
			the_str = the_str.insert(i + 1, " ")
	
	return the_str
