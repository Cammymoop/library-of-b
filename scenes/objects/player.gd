extends CharacterBody3D

var mouse_sensitivity: = 0.002

var joystick_h_sensetivity = 4
var joystick_v_sensetivity = 2

const MOVE_SPEED_BASE = 3.6
const MOVE_SPEED_MAX = 8.5
const JUMP_VELOCITY = 3.5

const SPEED_BUILDUP_DURATION = 30.0
const SPEED_BUILDUP_DELAY = 3.0

var speed_buildup_time: = 0.0
var my_move_speed: = MOVE_SPEED_BASE

const SPEED_LOSS_TIME = 0.85
const MIN_MOVEMENT = 0.006
var speed_loss_timer = 0.0

@onready var head: Node3D = $Head
@onready var camera: Camera3D = $Head/Camera3D
@onready var forward_cast: RayCast3D = $Head/Camera3D/ForwardCast

var last_room_center: = Vector2.ZERO

@onready var library: Node3D = $".."

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	update_cursor_vis()

func h_pos() -> Vector2:
	return Vector2(position.x, position.z)

func mouse_is_captured() -> bool:
	return Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED

func toggle_mouse_capture() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE if mouse_is_captured() else Input.MOUSE_MODE_CAPTURED
	update_cursor_vis()

func update_cursor_vis() -> void:
	$Cursor.visible = mouse_is_captured()

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		var e: = event as InputEventMouseButton
		if e.is_pressed():
			#if e.button_index == MOUSE_BUTTON_RIGHT or (e.button_index == MOUSE_BUTTON_LEFT and not mouse_is_captured()):
			if e.button_index == MOUSE_BUTTON_RIGHT:
				toggle_mouse_capture()
			if mouse_is_captured() and e.button_index == MOUSE_BUTTON_LEFT:
				do_book_click()
	
	if not mouse_is_captured():
		return
	
	if event is InputEventMouseMotion:
		camera.rotate_x(-event.relative.y * mouse_sensitivity)
		head.rotate_y(-event.relative.x * mouse_sensitivity)
		camera.rotation.x = clamp(camera.rotation.x, -PI/2, PI/2)

func do_book_click() -> void:
	forward_cast.force_raycast_update()
	if forward_cast.is_colliding():
		var node = forward_cast.get_collider()
		if node and node is Area3D:
			if node.has_signal("me_clicked"):
				node.me_clicked.emit()

func _physics_process(delta: float) -> void:
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
		library.books_per_frame = []

	var input_dir := Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	var direction := (head.transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * my_move_speed
		velocity.z = direction.z * my_move_speed
	else:
		velocity.x = move_toward(velocity.x, 0, my_move_speed)
		velocity.z = move_toward(velocity.z, 0, my_move_speed)
	
	var old_pos: = position
	move_and_slide()
	
	var h_delta = Vector3(old_pos.x - position.x, 0, old_pos.y - position.y).length()
	if h_delta < MIN_MOVEMENT:
		if my_move_speed > MOVE_SPEED_BASE:
			speed_loss_timer += delta
			if speed_loss_timer > SPEED_LOSS_TIME:
				speed_loss_timer = 0
				my_move_speed = MOVE_SPEED_BASE
				speed_buildup_time = 0
	else:
		speed_loss_timer = 0
		speed_buildup_time += delta
		var buildup = (speed_buildup_time - SPEED_BUILDUP_DELAY) / SPEED_BUILDUP_DURATION
		buildup = min(1, buildup)
		if buildup > 0:
			my_move_speed = MOVE_SPEED_BASE + (buildup * (MOVE_SPEED_MAX - MOVE_SPEED_BASE))

func _process(delta: float) -> void:
	joystick_look(delta)
	
	if Input.is_action_just_pressed("action"):
		do_book_click()
	
	if (h_pos() - last_room_center).length() > 8:
		var new_room_coords: Vector3i = library.get_room_coordinates_from_world(position)
		#prints("new coords", new_room_coords, (h_pos() - last_room_center).length())
		var new_room_center: Vector2 = library.tile_to_world(new_room_coords)
		if new_room_center != last_room_center:
			#prints(new_room_coords, (h_pos() - last_room_center).length(), "new dist", (h_pos() - new_room_center).length())
			#print(new_room_center)
			library.load_nearby(new_room_coords)
			last_room_center = new_room_center

func joystick_look(delta: float) -> void:
	var h_look = Input.get_axis("look_right", "look_left")
	head.rotate_y(h_look * joystick_h_sensetivity * delta)
	
	var v_look = Input.get_axis("look_down", "look_up")
	camera.rotate_x(v_look * joystick_v_sensetivity * delta)
	camera.rotation.x = clamp(camera.rotation.x, -PI/2, PI/2)
