extends Node3D

var out: = false

var animate_up: = false

var mat_a: StandardMaterial3D = null
var mat_b: StandardMaterial3D = null

func _ready() -> void:
	var b: MeshInstance3D = $book_openable/BookModel
	mat_a = b.get_surface_override_material(0)
	mat_b = b.get_surface_override_material(1)

func set_albedo_col(new_col: Color) -> void:
	#if not mat_a:
		#setup_mats()
	mat_a.albedo_color = new_col
	mat_b.albedo_color = new_col
	#for node in [$book_openable/BookModel, $book_openable/CoverHinge/BookCover]:
		#var m: = node as MeshInstance3D
		#var mat: = m.get_surface_override_material(0) as StandardMaterial3D
		#mat.albedo_color = new_col
		#mat = m.get_surface_override_material(1) as StandardMaterial3D
		#mat.albedo_color = new_col
	

func set_my_text(new_text: String) -> void:
	$book_openable/Label3D.text = new_text

func _on_book_pickable_input_event(camera: Node, event: InputEvent, position: Vector3, normal: Vector3, shape_idx: int) -> void:
	var e: = event as InputEventMouseButton
	if not e:
		return
	if not e.is_pressed() or e.button_index != MOUSE_BUTTON_LEFT:
		return
	
	am_clicked()

func am_clicked() -> void:
	$"../..".check_and_refresh_text()
	toggle_out()

func toggle_out() -> void:
	var ap: AnimationPlayer = $AnimationPlayer as AnimationPlayer
	#var speed: float = 1 if not out else -1
	if not out:
		ap.play("open")
		if animate_up:
			var ap2: = $AnimationPlayer2 as AnimationPlayer
			ap2.play("up")
		BookUtil.new_open(self)
	else:
		ap.play_backwards("open")
		if animate_up:
			var ap2: = $AnimationPlayer2 as AnimationPlayer
			ap2.play_backwards("up")
	out = not out

func close_me() -> void:
	if not out:
		return
	toggle_out()

func _on_book_pickable_me_clicked() -> void:
	am_clicked()
