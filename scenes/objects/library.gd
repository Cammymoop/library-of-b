extends Node3D

const room_scn = preload("res://scenes/objects/hex_room.tscn")
const flat_hall_scn = preload("res://scenes/objects/hall_flat.tscn")
const ramp_hall_scn = preload("res://scenes/objects/hall_up_v2.tscn")

const room_create_distance = 1
const room_drop_distance = 2
const room_despawn_distance = 6

const HEIGHT_STEP = 0.8894

var tri_basis_b: = Vector2(0, 1).rotated(-PI/6)
#var tile_cartesian_offset = Vector2(0.5, 1 / (2 * sqrt(3)))

var loaded_rooms: = []
var spawned_rooms: Array[Vector3i] = []
var spawned_halls: Array[Vector3i] = []

var spiral_index_hack: = 0

var books_this_frame: = 0
var books_per_frame: = []

var grid_zero: = Vector2i.ZERO
var grid_zero3: = Vector3i.ZERO
var zero_height: = 0

@onready var label: Label = $Control/Label

func _ready() -> void:
	var rand_low: int = abs(randi_range(0, 4194304))
	var rand_high: int = abs(randi_range(0, 65536))
	spiral_index_hack = rand_low + (rand_high << 22)
	spiral_index_hack = 0
	
	make_spiral_positions()
	#set_room_coordinates()
	load_nearby(Vector3i(0, 0, 0))

#func _process(delta: float) -> void:
	#if books_this_frame > 10 and books_this_frame / 2 not in books_per_frame:
		#books_per_frame.append(books_this_frame / 2)
		#update_bpf()
	#books_this_frame = 0

func update_bpf() -> void:
	label.text = "\n".join(books_per_frame)

func find_assign_room_coord(room: Node3D) -> void:
	assign_room_coord(room, get_room_coordinates_from_world(room.global_position))

func assign_room_coord(room: Node3D, vec_coord: Vector3i) -> void:
	room.vec_coord = vec_coord
	var spiral_info = get_room_spiral_pos_index(vec_coord)
	room.spiral_pos = spiral_info[0]
	room.spiral_id = spiral_info[1]
	#print(room.spiral_id)
	room.room_id = get_in_spiral_index(vec_coord, room.spiral_pos)
	#if room.spiral_id != 0:
		#prints("assigned sp", room.spiral_pos, "tile", vec_coord, room.room_id)
	#print(room.room_id)

func load_nearby(room_pos: Vector3i) -> void:
	var new_loaded: = []
	for r in loaded_rooms:
		var r_pos: Vector3i = r.vec_coord
		var distance: = tile_manhattan_dist(r_pos, room_pos)
		if distance >= room_drop_distance:
			r.unload_books()
		else:
			new_loaded.append(r)
	
	loaded_rooms = new_loaded
	
	if room_pos not in spawned_rooms:
		spawn_new_room(room_pos)
	var spawnable_positions: = adjacent_sight4(room_pos)
	for r_pos in spawnable_positions:
		if r_pos not in spawned_rooms:
			spawn_new_room(r_pos)
	
	#prints("rooms spawned:", $Rooms.get_child_count())
	for r in $Rooms.get_children():
		if r.vec_coord == room_pos:
			pass#prints("in room", r.room_id, [r.spiral_pos, r.spiral_id], r.vec_coord)
		if r in loaded_rooms:
			#prints(r.vec_coord, "already loaded")
			continue
		var r_pos: Vector3i = r.vec_coord
		var distance: = tile_manhattan_dist(r_pos, room_pos)
		if distance <= room_create_distance:
			r.load_books()
			loaded_rooms.append(r)
		elif r_pos not in spawnable_positions and distance > room_despawn_distance:
			spawned_rooms.erase(r_pos)
			r.queue_free()
	
	#if len(loaded_rooms) < 1:
		#prints("no loaded rooms", room_pos)

var hall_rotations: Array[float] = [(-2 * PI) / 3, 0, (2 * PI) / 3]
func spawn_new_room(at_tile_pos: Vector3i) -> void:
	spawned_rooms.append(at_tile_pos)
	var new_room = room_scn.instantiate()
	var world_pos: = w2_to_w3(tile_to_world(at_tile_pos))
	world_pos.y = tile_height(at_tile_pos)
	$Rooms.add_child(new_room)
	new_room.global_position = world_pos
	if at_tile_pos.z > 0:
		new_room.rotate_y(PI/3)
	assign_room_coord(new_room, at_tile_pos)
	if new_room.room_id == 0:
		new_room.floor_circle()
	
	var halls: = get_hall_ident(at_tile_pos)
	for h in halls:
		if h in spawned_halls:
			continue
		spawned_halls.append(h)
		var h_pos = get_edge_centers(at_tile_pos)
		var vert: = edge_vertical_change(at_tile_pos, h.z)
		var new_hall: = (flat_hall_scn if vert == 0 else ramp_hall_scn).instantiate()
		$Halls.add_child(new_hall)
		new_hall.global_position = c_to_w3(tri_to_cartesian(h_pos[h.z]))
		var angle: = hall_rotations[h.z] + (PI * at_tile_pos.z)
		new_hall.rotate_y(angle)
		new_hall.global_position.y = tile_height(at_tile_pos)
		if vert < 0:
			new_hall.position.y -= HEIGHT_STEP
			new_hall.rotate_y(PI)

func get_hall_ident(tile_pos: Vector3i) -> Array[Vector3i]:
	var arr: Array[Vector3i] = [Vector3i(tile_pos.x, tile_pos.y, 0)]
	if tile_pos.z == 0:
		arr.append(Vector3i(tile_pos.x, tile_pos.y, 1))
		arr.append(Vector3i(tile_pos.x, tile_pos.y, 2))
	else:
		arr.append(Vector3i(tile_pos.x, tile_pos.y + 1, 1))
		arr.append(Vector3i(tile_pos.x + 1, tile_pos.y, 2))
	return arr


# Room Layout Functions
func get_room_spiral_pos_index(room_coord: Vector3i) -> Array:
	var the_spiral_pos = what_spiral_am_i(tile_to_tri(room_coord))
	if typeof(the_spiral_pos) == TYPE_BOOL and the_spiral_pos == false:
		print("empty room!!")
		return [Vector2i.ZERO, -6]
	
	return [Vector2i(the_spiral_pos.round()), spiral_center_to_index(the_spiral_pos)]

func get_in_spiral_index(room_coord: Vector3i, spiral_pos: Vector2i, debug: = false) -> int:
	var relative_point = room_coord - spiral_pos_to_tile_pos(spiral_pos)
	if debug:
		prints("room", room_coord, relative_point)
	
	var sp_pos = spiral_positions_b if spiral_pos.y % 2 == 1 else spiral_positions
	return sp_pos.find(relative_point)

func is_room_emtpy(room_coord: Vector3i) -> bool:
	return false

# Spiral Functions
const UNIT_VEC: Array[Vector3i] = [Vector3i(1, 0, 0), Vector3i(0, 1, 0), Vector3i(0, 0, 1)]
var side_movements: = [
	{0: 1, 1:  1, 2:  0, 3: -1, 4: -1, 5: 0,}, # X
	{0: 0, 1: -1, 2: -1, 3:  0, 4:  1, 5: 1,}, # Y
	{0: 1, 1:  0, 2: -1, 3: -1, 4:  0, 5: 1,}, # Z
]
var side_meta_move: = [ [0, 2], [1, 0], [2, 1] ]

# Basis vectors for the spiral meta-grid (hexagons)
# these bases are in the skewed tri coordinate space, not cartesian
var spiral_basis_x: = Vector2(10, 8)
var spiral_basis_y: = Vector2(8, -18.5)
var spiral_basis: = Transform2D(spiral_basis_x, spiral_basis_y, Vector2.ZERO)

var spiral_b_center_offset: = Vector2(-1, 0)

# In a spiral fill rings out from index 1 to the last index and then index 0 (for geometrical reasons)
func next_spiral_dir(ring_size: int, ring_index: int) -> Vector3i:
	if ring_size < 1:
		print_debug("too small of a ring")
		return Vector3i.ZERO
	
	# (12*size) - 6 mod <num_rooms> is 0
	if ring_index >= (12 * ring_size) - 6:
		# Leave this ring for the next one
		return Vector3i.ZERO
	
	var side_length: = (ring_size * 2) - 1
	var side_index: = floori(ring_index / side_length)
	var inner_index: = ring_index % side_length
	
	var movement_axis: int = side_meta_move[side_index % 3][inner_index % 2]
	return UNIT_VEC[movement_axis] * side_movements[movement_axis][side_index]

var spiral_positions: = []
var spiral_positions_b: = []

func make_spiral_positions() -> void:
	var cur_pos: = Vector3i.ZERO
	var cur_size: = 1
	var in_ring_index: = 1 # 0 based, but start on index 1
	for i in 512:
		spiral_positions.append(cur_pos)
		spiral_positions_b.append(-cur_pos)
		
		var next_move: = next_spiral_dir(cur_size, in_ring_index)
		var jump_ring: = next_move == Vector3i.ZERO
		if jump_ring:
			next_move = Vector3i(0, 1, 0)
		cur_pos += next_move
		var moving_dir: = next_move.x + next_move.y
		#adjust z for moving between squares
		if moving_dir > 0:
			cur_pos.z = 0
		elif moving_dir < 0:
			cur_pos.z = 1
		
		if cur_pos.z < 0 or cur_pos.z > 1:
			print_debug("oops ran out of real space while writing spiral positions")
			prints(cur_pos, i, cur_size, in_ring_index, next_move, jump_ring)
			return
		in_ring_index += 1
		
		if jump_ring:
			cur_size += 1
			in_ring_index = 1
	
	#for i in 64:
		#print(spiral_positions_b.slice(i * 8, (i+1) * 8))
	
	# add final positions
	# oops already did 512 this wasn't necessary
	#spiral_positions.append(cur_pos)
	#spiral_positions_b.append(-cur_pos)

func tri_point_to_spiral_grid_point(tri_point: Vector2) -> Vector2:
	return spiral_basis.affine_inverse().basis_xform(tri_point)

func spiral_grid_point_to_tri(spiral_point: Vector2) -> Vector2:
	return spiral_basis.basis_xform(spiral_point)

func spiral_pos_to_tile_pos(spiral_pos: Vector2i) -> Vector3i:
	var tri_point: = spiral_grid_point_to_tri(spiral_pos).floor()
	var tile_pos: = Vector3i(tri_point.x, tri_point.y, 0)
	if spiral_pos.y % 2 == 1:
		tile_pos.z = 1
		tile_pos.x += spiral_b_center_offset.x
		tile_pos.y += spiral_b_center_offset.y
	
	return tile_pos

var last_closest_info: = []
func closest_spirals(tri_point: Vector2) -> Array:
	last_closest_info = []
	var spiral_point: = tri_point_to_spiral_grid_point(tri_point)
	var center_points: = floors_and_ceils(spiral_point)
	
	return center_points
	#last_closest_info.append(spiral_point)
	#last_closest_info.append(center_points)
	#
	#var min_dist: = 99.0
	#var min_i: = 69
	#var next_min_dist: = 99.0
	#var next_min_i: = 69
	#for i in len(center_points):
		#var p: = center_points[i]
		#var dist: = (spiral_point - p).length()
		#last_closest_info.append(dist)
		#if dist < min_dist:
			#next_min_dist = min_dist
			#next_min_i = min_i
			#min_dist = dist
			#min_i = i
		#elif dist < next_min_dist:
			#next_min_dist = dist
			#next_min_i = i
	#
	#return [center_points[min_i], center_points[next_min_i]]

func what_spiral_am_i(tri_point: Vector2):
	var closest_sp = closest_spirals(tri_point)
	
	var z: = tri_point_lr(tri_point)
	var tri_floored: = tri_point.floor()
	
	for point: Vector2 in closest_sp:
		if am_i_in_this_spiral(tri_floored, z, point):
			return point
	
	prints("not in a spiral", tri_floored, closest_sp)
	print(last_closest_info)
	
	return false

var once: = true
# use a floored tri point and a z coordinate
func am_i_in_this_spiral(tri_grid_point: Vector2, z_pos: int, spiral_center: Vector2) -> bool:
	var y: int = roundi(spiral_center.y)
	var is_spiral_a: = y % 2 == 0
	var sp_pos = spiral_positions if is_spiral_a else spiral_positions_b
	var sp_tri_pos = spiral_grid_point_to_tri(spiral_center).floor()
	
	if not is_spiral_a:
		sp_tri_pos += spiral_b_center_offset
	if once and spiral_center.round() == Vector2(0, 1):
		once = false
		print(sp_tri_pos)
	
	var relative_point = (tri_grid_point - sp_tri_pos).round()
	var rel_z: int = z_pos - (0 if is_spiral_a else 1)
	if is_spiral_a:
		if rel_z < 0 or rel_z > 1:
			print_debug("Spiral a, rel z out of range")
	else:
		if rel_z < -1 or rel_z > 0:
			print_debug("Spiral b, rel z out of range")
	
	var ivec: = Vector3i(roundi(relative_point.x), roundi(relative_point.y), rel_z)
	if ivec in sp_pos:
		return true
	
	#prints("point not in either spiral!", is_spiral_a, ivec, tri_grid_point, z_pos, sp_pos)
	return false

func spiral_center_to_index(spiral_center: Vector2) -> int:
	var ring: int = hex_veci_length(spiral_center)
	if ring == 0:
		return 0
	
	return ring_index(ring, Vector2i(spiral_center))

var facsums: = [0]
func calculate_facsum(x: int) -> int:
	var next: = len(facsums)
	
	if x < next:
		return facsums[x]
	
	var last_val: int = facsums[next - 1]
	while x >= next:
		last_val += next
		facsums.append(last_val)
		next += 1
	
	return facsums[x]

func ring_index(ring: int, point: Vector2i) -> int:
	if ring == 0:
		return 0
	var starting_ring_index: int = (calculate_facsum(ring - 1) * 6) + 1
	var per_side: = ring
	
	var local_index: = 0
	# find indices by quadrant
	if point.y < 0 and point.x >= 0:
		local_index = point.x
	elif point.y >= 0 and point.x > 0:
		local_index += per_side
		if point.x < ring:
			local_index += per_side
			local_index += ring - point.x
		else:
			local_index += point.y
	elif point.y > 0 and point.x <= 0:
		local_index += 3 * per_side
		local_index += -point.x
	else: # point.y <= 0 and point.x < 0
		local_index += 4 * per_side
		if point.x > -ring:
			local_index += per_side
			local_index += ring - (-point.x)
		else:
			local_index += -point.y
	
	prints(point, ring, local_index, starting_ring_index)
	return local_index + starting_ring_index



# Coordinate functions
func c_to_w(cart_point: Vector2) -> Vector2:
	return cart_point * unit_size
func w_to_c(world_point: Vector2) -> Vector2:
	return world_point / unit_size
func w3_to_c(world_point: Vector3) -> Vector2:
	return Vector2(world_point.x, world_point.z) / unit_size
func w2_to_w3(world_point: Vector2, y: float = 0) -> Vector3:
	return Vector3(world_point.x, y, world_point.y)
func c_to_w3(cart_point: Vector2) -> Vector3:
	return Vector3(cart_point.x, 0, cart_point.y) * unit_size

func get_room_coordinates_from_world(world_pos: Vector3) -> Vector3i:
	var pos2: = w3_to_c(world_pos)
	pos2 = cartesian_to_tri(pos2)
	var lr_point: = tri_point_lr(pos2)
	pos2 = pos2.floor()
	
	return Vector3i(pos2.x, pos2.y, lr_point)


var unit_size: float = 24
func cartesian_to_tri(point: Vector2) -> Vector2:
	var tri_point: = Vector2.ZERO
	tri_point.y = point.y/tri_basis_b.y
	tri_point.x = point.x - (tri_basis_b.x * tri_point.y)
	
	return tri_point

func tri_to_cartesian(tri_point: Vector2) -> Vector2:
	var cart_point: = Vector2.ZERO
	cart_point.y = tri_point.y * tri_basis_b.y
	cart_point.x = tri_point.x + (tri_basis_b.x * tri_point.y)
	
	return cart_point

func tile_to_tri(tile_pos: Vector3i) -> Vector2:
	var corner_pos: = Vector2(tile_pos.x, tile_pos.y)
	const tri_offset = Vector2(0.353553, 0.353553)
	if tile_pos.z == 0:
		return corner_pos + tri_offset
	else:
		return corner_pos + Vector2.ONE - tri_offset

func tile_to_cartesian(tile_pos: Vector3i) -> Vector2:
	var corner_pos: Vector2 = tri_to_cartesian(Vector2(tile_pos.x, tile_pos.y))
	
	if tile_pos.z == 0:
		corner_pos += Vector2(0.5, 1/(2 * sqrt(3)))
	else:
		corner_pos += Vector2(1, sqrt(3) / 3)
	return corner_pos

func tile_to_world(tile_pos: Vector3i) -> Vector2:
	return c_to_w(tile_to_cartesian(tile_pos))

func adjacent_tiles(tile_pos: Vector3i) -> Array[Vector3i]:
	var alt: = 1 - tile_pos.z
	var add: = 1 if tile_pos.z > 0 else -1
	return [
		Vector3i(tile_pos.x,       tile_pos.y,       alt),
		Vector3i(tile_pos.x + add, tile_pos.y,       alt),
		Vector3i(tile_pos.x,       tile_pos.y + add, alt),
	]

func adjacent_2(tile_pos: Vector3i) -> Array[Vector3i]:
	var alt: = 1 - tile_pos.z
	var add: = 1 if tile_pos.z > 0 else -1
	return [
		Vector3i(tile_pos.x,       tile_pos.y,       alt),
		Vector3i(tile_pos.x + add, tile_pos.y,       alt),
		Vector3i(tile_pos.x,       tile_pos.y + add, alt),
		
		Vector3i(tile_pos.x - 1, tile_pos.y,     tile_pos.z),
		Vector3i(tile_pos.x,     tile_pos.y - 1, tile_pos.z),
		Vector3i(tile_pos.x + 1, tile_pos.y,     tile_pos.z),
		Vector3i(tile_pos.x,     tile_pos.y + 1, tile_pos.z),
		Vector3i(tile_pos.x - 1, tile_pos.y + 1, tile_pos.z),
		Vector3i(tile_pos.x + 1, tile_pos.y - 1, tile_pos.z),
	]

func adjacent_sight4(tile_pos: Vector3i) -> Array[Vector3i]:
	var alt: = 1 - tile_pos.z
	var add: = 1 if tile_pos.z > 0 else -1
	return [
		Vector3i(tile_pos.x,       tile_pos.y,       alt),
		Vector3i(tile_pos.x + add, tile_pos.y,       alt),
		Vector3i(tile_pos.x,       tile_pos.y + add, alt),
		
		Vector3i(tile_pos.x - 1, tile_pos.y,     tile_pos.z),
		Vector3i(tile_pos.x,     tile_pos.y - 1, tile_pos.z),
		Vector3i(tile_pos.x + 1, tile_pos.y,     tile_pos.z),
		Vector3i(tile_pos.x,     tile_pos.y + 1, tile_pos.z),
		Vector3i(tile_pos.x - 1, tile_pos.y + 1, tile_pos.z),
		Vector3i(tile_pos.x + 1, tile_pos.y - 1, tile_pos.z),
		
		Vector3i(tile_pos.x - 2, tile_pos.y,     alt),
		Vector3i(tile_pos.x,     tile_pos.y - 2, alt),
		Vector3i(tile_pos.x + 2, tile_pos.y,     alt),
		Vector3i(tile_pos.x,     tile_pos.y + 2, alt),
		
		Vector3i(tile_pos.x - 2, tile_pos.y,     tile_pos.z),
		Vector3i(tile_pos.x,     tile_pos.y - 2, tile_pos.z),
		Vector3i(tile_pos.x + 2, tile_pos.y,     tile_pos.z),
		Vector3i(tile_pos.x,     tile_pos.y + 2, tile_pos.z),
		
		Vector3i(tile_pos.x - 2, tile_pos.y + 1, alt),
		Vector3i(tile_pos.x + 1, tile_pos.y - 2, alt),
		Vector3i(tile_pos.x - 2, tile_pos.y + 2, tile_pos.z),
		Vector3i(tile_pos.x + 2, tile_pos.y - 2, tile_pos.z),
	]

# get the positions of the centers of each edge of a tile in tri coordinate space
func get_edge_centers(tile_pos: Vector3i) -> Array:
	var tl: = Vector2(tile_pos.x, tile_pos.y)
	return [
		tl + Vector2(0.5, 0.5),
		tl + Vector2(0.5, tile_pos.z),
		tl + Vector2(tile_pos.z, 0.5)
	]

func world_edge_centers(tile_pos: Vector3i) -> Array:
	var edges: = get_edge_centers(tile_pos)
	for i in len(edges):
		edges[i] = c_to_w(tri_to_cartesian(edges[i]))
	return edges

func edge_vertical_change(tile_pos: Vector3i, index: int) -> int:
	var h1: = tile_height(tile_pos)
	var add: = 1 if tile_pos.z > 0 else -1
	tile_pos.z = 1 - tile_pos.z
	if index == 1:
		tile_pos.y += add
	elif index == 2:
		tile_pos.x += add
	
	return sign(tile_height(tile_pos) - h1)

func tile_height(tile_pos: Vector3i) -> float:
	var world_tile: = tile_pos - grid_zero3
	var d = tile_manhattan_abs(tile_pos)
	return (d - zero_height) * HEIGHT_STEP

# thanks red blob https://simblob.blogspot.com/2007/06/distances-on-triangular-grid.html
func tile_manhattan_dist(tile_pos_a: Vector3i, tile_pos_b: Vector3i) -> int:
	var alt_a: = conv_to_alt(tile_pos_a)
	var alt_b: = conv_to_alt(tile_pos_b)
	
	var acc_diff = abs(alt_a.x - alt_b.x)
	acc_diff += abs(alt_a.y - alt_b.y)
	acc_diff += abs(alt_a.z - alt_b.z)
	return acc_diff

func tile_manhattan_abs(tile_pos: Vector3i) -> int:
	return abs(tile_pos.x) + abs(tile_pos.y) + abs(tile_pos.x + tile_pos.y + tile_pos.z)

func conv_to_alt(tile_pos: Vector3i) -> Vector3i:
	var new_vec: = Vector3i(tile_pos.y, tile_pos.x, 0)
	new_vec += Vector3i(0, 0, tile_pos.x + tile_pos.y + tile_pos.z)
	return new_vec

func tri_point_lr(tri_point: Vector2) -> int:
	var iqp: = tri_point.posmod(1.0) # in-quad point (fractional part)
	return floori(iqp.x + iqp.y)

func floors_and_ceils(vec2: Vector2) -> Array[Vector2]:
	var points: Array[Vector2] = [vec2.floor()]
	points.append(Vector2(ceilf(vec2.x), floorf(vec2.y)))
	points.append(Vector2(floorf(vec2.x), ceilf(vec2.y)))
	points.append(vec2.ceil())
	return points

# I'm subtracted in the second term because of how I aligned my hex axial position axes
func hex_veci_length(hex_point: Vector2) -> int:
	var h: Vector2i = hex_point.floor()
	return (absi(h.x) + abs(h.x - h.y) + abs(h.y)) / 2
