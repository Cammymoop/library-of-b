extends Node3D

@export var room_id: = -1
@export var spiral_id: = -1

var vec_coord: Vector3i = Vector3i.ZERO
var spiral_pos: Vector2i = Vector2i.ZERO

@onready var wall1: Node3D = $WallAt1/WallInstance
@onready var wall2: Node3D = $WallaAt2/WallInstance
@onready var wall3: Node3D = $WallAt3/WallInstance

const circle_floor_scn = preload("res://scenes/objects/circle_floor.tscn")
var floor_node: Node3D = null

func unload_books() -> void:
	#prints("unloading", vec_coord)
	for w in [wall1, wall2, wall3]:
		var shelves = w.get_node("Shelves")
		for bookshelf in shelves.get_children():
			await get_tree().process_frame
			bookshelf.unload_books()

func load_books() -> void:
	#prints("loading", vec_coord)
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	for w in [wall1, wall2, wall3]:
		var shelves = w.get_node("Shelves")
		for bookshelf in shelves.get_children():
			await get_tree().process_frame
			bookshelf.load_me()

func clear_floor() -> void:
	if floor_node:
		remove_child(floor_node)
		floor_node.queue_free()
	floor_node = null

func floor_circle() -> void:
	clear_floor()
	floor_node = circle_floor_scn.instantiate()
	add_child(floor_node)
	floor_node.position.y = -0.22
