extends Node3D

const BOOKS_26 = preload("res://scenes/objects/books_26.tscn")

@onready var shelf_parents: Array[Node] = [$Shelf, $Shelf2, $Shelf3]
@onready var low_lods: Array[Node] = [$Shelf/LowBooks, $Shelf2/LowBooks2, $Shelf3/LowBooks3]

var is_pre_populated: = false

var is_populated: = false

var in_out_random: = 0.01
var darken_random: = 0.17
var rand_scale: = 0.03
var rand_rotation: = PI/40

var room_id: = -1
var spiral_id: = -1
var bookshelf_index: = -1

func _ready() -> void:
	#randomize setup
	if is_pre_populated:
		randomize_all_shelves()

func load_me() -> void:
	populate_shelves()

func get_my_shelf_index() -> void:
	var wall_index = int(get_node("../../..").name.trim_prefix("WallAt")) - 1
	var in_wall_i = get_index()
	bookshelf_index = (wall_index * 3) + in_wall_i
	
	if bookshelf_index == 8 and len(low_lods) > 2:
		low_lods[2].visible = false
		low_lods = [low_lods[0], low_lods[1]]

func get_my_room() -> Node:
	return get_node("../../../..")

func get_or_make_new_shelf() -> Node3D:
	if ShelfCache.shelves_available > 0:
		return ShelfCache.get_shelf()
	return BOOKS_26.instantiate()

func populate_shelves() -> void:
	if is_populated:
		return
	is_populated = true
	var the_room: Node = get_my_room()
	room_id = the_room.room_id
	spiral_id = the_room.spiral_id
	
	get_my_shelf_index()
	
	if bookshelf_index == 8:
		shelf_parents = [shelf_parents[0], shelf_parents[1]]
	
	for i: int in len(shelf_parents):
		var shelf_parent: Node = shelf_parents[i]
		var new_books = get_or_make_new_shelf()
		new_books.dirty_text = true
		new_books.h_index = i
		shelf_parent.add_child(new_books, true)
		new_books.name = "Books"
	hide_low_lod()
	
	randomize_all_shelves()

func unload_books() -> void:
	is_populated = false
	for s in shelf_parents:
		var books: Node = s.get_node_or_null("Books")
		if books:
			ShelfCache.take_shelf(books)
	
	show_low_lod()

func hide_low_lod() -> void:
	for low in low_lods:
		low.visible = false

func show_low_lod() -> void:
	for low in low_lods:
		low.visible = true

func randomize_all_shelves() -> void:
	return
	for shelf_p: Node in shelf_parents:
		var one_shelf: Node = shelf_p.get_node_or_null("Books")
		if not one_shelf or one_shelf.randomized:
			continue
		one_shelf.randomized = true
		randomize_books_in(one_shelf)
		#for i in range(8):
			#await get_tree().process_frame

func randomize_books_in(book_set: Node) -> void:
	if book_set == null:
		return
	
	var book_markers: = book_set.get_children()
		
	var lib: Node = get_my_room().get_node("../..")
	
	for bm: Marker3D in book_markers:
		bm.position.z += randf() * in_out_random
		
		bm.get_child(0).set_albedo_col(rand_color())
		bm.scale = Vector3.ONE * (1 - rand_scale + (randf() * rand_scale))
		
		var rotation_amt = -(rand_rotation/2) + (randf() * rand_rotation)
		bm.rotate_y(rotation_amt)
		
		lib.books_this_frame += 1
		
		#if bm.get_index() % 3 == 2:
			#await get_tree().process_frame

func rand_color() -> Color:
	var amt = 1 - darken_random + (randf() * darken_random)
	return Color(amt, amt, amt)
