extends Node3D

var h_index: = -1
var randomized: = false
var dirty_text: = true

func check_and_refresh_text() -> void:
	if not dirty_text:
		return
	
	if h_index < 0:
		print_debug("h_index not set")
	
	var the_bookshelf = get_node("../..")
	var room_id: int = the_bookshelf.room_id
	var spiral_id: int = the_bookshelf.spiral_id
	
	var first_book_index: int = the_bookshelf.bookshelf_index * 78
	first_book_index += 26 * h_index
	
	dirty_text = false
	for i in get_child_count():
		var a_book: Node = get_child(i).get_child(0)
		a_book.animate_up = h_index == 0
		a_book.set_my_text(BookText.get_book_text(room_id, spiral_id, first_book_index + i))
